package com.lesson.vadim.weatherpractic.data;

import org.json.JSONObject;

/**
 * Created by Vadim on 28.10.2015.
 */
public class Units implements JSONApp {
    private String temperature;

    public String getTemperature() {
        return temperature;
    }

    @Override
    public void populate(JSONObject data) {
        temperature = data.optString("temperature");
    }
}
