package com.lesson.vadim.weatherpractic.retrofit;

import android.widget.Toast;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public abstract class CallBack<T> implements Callback<T> {

    @Override
    public void success(T o, Response response) {
        if(response.getStatus()/10==20){
            succes(o);
        }else {
            Toast.makeText(App.getInstance(), "Error connect", Toast.LENGTH_SHORT).show();}
    }

    public abstract void succes(T o);
    @Override
    public void failure(RetrofitError error) {
        Toast.makeText(App.getInstance(), "Error connect", Toast.LENGTH_SHORT).show();
    }
}
