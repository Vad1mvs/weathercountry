package com.lesson.vadim.weatherpractic.data;

import org.json.JSONObject;

/**
 * Created by Vadim on 28.10.2015.
 */
public interface JSONApp {
    void populate(JSONObject data);
}
