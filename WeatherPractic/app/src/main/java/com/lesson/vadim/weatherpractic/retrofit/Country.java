package com.lesson.vadim.weatherpractic.retrofit;


public class Country {
    String name;
    String capital;
    String region;
    String subregion;
    String population;
    String area;
    public String getArea() {
        return area;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public String getRegion() {
        return region;
    }

    public String getSubregion() {
        return subregion;
    }

    public String getPopulation() {
        return population;
    }

    @Override
    public String toString() {
        return("County: "+ name+"Capital: "+capital+"Region: "+region+"Subregion: "+subregion+"Population: "+population);
    }
}
