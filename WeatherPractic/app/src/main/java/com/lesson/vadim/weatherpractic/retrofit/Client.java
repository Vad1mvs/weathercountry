package com.lesson.vadim.weatherpractic.retrofit;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by Vadim on 24.09.2015.
 */
public class Client {
    static Client instance;
    static API api;
    String url = "http://restcountries.eu/";

    public static Client getInstance() {
        if (instance == null) {
            instance = new Client();
        }
        return instance;
    }

    public static API getApi() {
        return getInstance().api;

    }

    private Client() {

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(url).build();
        api = restAdapter.create(API.class);
    }

    public interface API {
        @GET("/rest/v1/all")
        void getCountryTwo(Callback<List<Country>> cbb);
    }
}
