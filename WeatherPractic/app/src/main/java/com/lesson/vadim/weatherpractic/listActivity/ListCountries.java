package com.lesson.vadim.weatherpractic.listActivity;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;


import com.lesson.vadim.weatherpractic.R;
import com.lesson.vadim.weatherpractic.retrofit.AdapterCountry;
import com.lesson.vadim.weatherpractic.retrofit.CallBack;
import com.lesson.vadim.weatherpractic.retrofit.Client;
import com.lesson.vadim.weatherpractic.retrofit.Country;

import java.util.List;

import retrofit.RetrofitError;

public class ListCountries extends ListActivity {
    private static final String API_KEY = "api_key" ;
    List<Country> countryListTwo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_countries);
        Client.getApi().getCountryTwo(new CallBack<List<Country>>() {
            @Override
            public void succes(final List<Country> countries) {
                countryListTwo = countries;
                AdapterCountry adapterCountry = new AdapterCountry(getApplicationContext(), R.layout.item, countryListTwo);
                setListAdapter(adapterCountry);


                AdapterView.OnItemClickListener itemListener = new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        Intent intent = new Intent(ListCountries.this, Information.class);
                        countryListTwo = countries;
                        intent.putExtra("name", countries.get(position).getName());
                        intent.putExtra("capital", countries.get(position).getCapital());
                        intent.putExtra("region", countries.get(position).getRegion());
                        intent.putExtra("subregion", countries.get(position).getSubregion());
                        intent.putExtra("population", countries.get(position).getPopulation());
                        intent.putExtra("area", countries.get(position).getArea());
                        startActivity(intent);

                    }
                };
                getListView().setOnItemClickListener(itemListener);
            }


            @Override
            public void failure(RetrofitError error) {

            }

        });


    }}
