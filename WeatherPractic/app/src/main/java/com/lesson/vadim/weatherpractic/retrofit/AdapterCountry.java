package com.lesson.vadim.weatherpractic.retrofit;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lesson.vadim.weatherpractic.R;

import java.util.List;

public class AdapterCountry extends ArrayAdapter<Country> {

    private Context context;
    List<Country> countryListTwo;

    public AdapterCountry(Context context, int resource, List<Country> objects) {
        super(context, resource, objects);
        this.context = context;
        this.countryListTwo = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item,parent,false);
        Country country = countryListTwo.get(position);
        TextView tv = (TextView) view.findViewById(R.id.tv);
        tv.setText(country.getName());

        return view;
    }
}
