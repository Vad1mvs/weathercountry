package com.lesson.vadim.weatherpractic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lesson.vadim.weatherpractic.listActivity.ListCountries;
import com.lesson.vadim.weatherpractic.data.Channel;
import com.lesson.vadim.weatherpractic.data.Item;
import com.lesson.vadim.weatherpractic.retrofit.CallBack;
import com.lesson.vadim.weatherpractic.retrofit.Client;
import com.lesson.vadim.weatherpractic.retrofit.Country;
import com.lesson.vadim.weatherpractic.service.WeatherService;
import com.lesson.vadim.weatherpractic.service.WeatherServiceCallback;

import java.util.List;

import retrofit.RetrofitError;


public class MainActivity extends Activity implements WeatherServiceCallback {
    private ImageView weatherImageView;
    private TextView tvTemperature,tvCondition,tvLocation;
    private WeatherService service;
    private Button btnSearch, button;
    private EditText et;
    List<Country> countryListTwo;
    private String cap = "", cont = "";
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        btnSearch = (Button)findViewById(R.id.btnSearch);
        et = (EditText)findViewById(R.id.et);
        button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListCountries.class);
                startActivity(intent);
            }
        });
        weatherImageView = (ImageView)findViewById(R.id.weatherImageView);
        tvTemperature = (TextView)findViewById(R.id.tvTemperature);
        tvCondition = (TextView)findViewById(R.id.tvCondition);
        tvLocation = (TextView)findViewById(R.id.tvLocation);
        service = new WeatherService(this);
        dialog = new ProgressDialog(this);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Client.getApi().getCountryTwo(new CallBack<List<Country>>() {
            @Override
            public void succes(final List<Country> countries) {
                dialog.setMessage("Load One...");
                dialog.show();
                countryListTwo = countries;
                        for (int i = 0; i < countries.size(); i++) {
                            cont = countries.get(i).getName();
                            cap = countries.get(i).getCapital();
                            if (et.getText().toString().contains(cont)) {
                                Toast.makeText(MainActivity.this, "Excellent", Toast.LENGTH_SHORT).show();
                                dialog.setMessage("Load Two...");
                                dialog.show();
                                service.refreshWeather(cap);
                                break;
                            } else {
                            }
                        }
            }
            @Override
            public void failure(RetrofitError error) {
            }
        });

            }
        });

    }

    @Override
    public void serviceSuccess(Channel channel) {
        dialog.hide();
        Item item = channel.getItem();

        int resourceId = getResources().getIdentifier("drawable/icon_" + item.getCondition().getCode(), null, getPackageName());
        @SuppressWarnings("deprecation")
        Drawable weatherIconDrawable = getResources().getDrawable(resourceId);
        weatherImageView.setImageDrawable(weatherIconDrawable);


        String temperatureLabel = getResources().getString(R.string.temperature_output, item.getCondition().getTemperature(), channel.getUnits().getTemperature());

        tvTemperature.setText(temperatureLabel);
        tvCondition.setText(item.getCondition().getDescription());
        tvLocation.setText(channel.getLocation());
    }

    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();

    }

}
