package com.lesson.vadim.weatherpractic.data;

import org.json.JSONObject;

/**
 * Created by Vadim on 28.10.2015.
 */
public class Condition implements JSONApp {
    private int code;
    private  int temperature;
    private String description;

    public int getCode() {
        return code;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void populate(JSONObject data) {
        code = data.optInt("code");
        temperature = data.optInt("temp");
        description = data.optString("text");
    }
}
