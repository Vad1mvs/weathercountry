package com.lesson.vadim.weatherpractic.listActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.lesson.vadim.weatherpractic.R;


public class Information extends Activity {
    private TextView tv, tv2, tv3, tv4, tv5, tv6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information);

        tv = (TextView)findViewById(R.id.tv);
        tv2 = (TextView)findViewById(R.id.tv2);
        tv3 = (TextView)findViewById(R.id.tv3);
        tv4 = (TextView)findViewById(R.id.tv4);
        tv5 = (TextView)findViewById(R.id.tv5);
        tv6 = (TextView)findViewById(R.id.tv6);
        Intent intent = getIntent();

        String tvName = intent.getStringExtra("name");
        String tvCapital = intent.getStringExtra("capital");
        String tvRegion = intent.getStringExtra("region");
        String tvSubregion = intent.getStringExtra("subregion");
        String tvPopulation = intent.getStringExtra("population");
        String tvArea = intent.getStringExtra("area");

        tv.setText(tvName);
        tv2.setText(tvCapital);
        tv3.setText(tvRegion);
        tv4.setText(tvSubregion);
        tv5.setText(tvPopulation);
        tv6.setText(tvArea);

    }
}
