package com.lesson.vadim.weatherpractic.data;

import org.json.JSONObject;

/**
 * Created by Vadim on 28.10.2015.
 */
public class Item implements JSONApp {
    private Condition condition;
    @Override
    public void populate(JSONObject data) {
        condition = new Condition();
        condition.populate(data.optJSONObject("condition"));

    }

    public Condition getCondition() {
        return condition;
    }
}
