package com.lesson.vadim.weatherpractic.service;

import com.lesson.vadim.weatherpractic.data.Channel;


public interface WeatherServiceCallback {
    void serviceSuccess (Channel channel);
    void serviceFailure (Exception exception);
}
